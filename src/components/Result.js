import React from 'react';

function Result({res}) {
    const link = res.QuestionReference && res.LokSabha ? `http://loksabhaph.nic.in/Questions/QResult15.aspx?qref=${res.QuestionReference}&lsno=${res.LokSabha}` : "http://loksabhaph.nic.in/Questions/Qtextsearch.aspx" 
    return <div>
        <a href={link} target="_blank" rel="noopener noreferrer">
        Question {res.File} by {res.Name}
        </a>
        <hr/>
        </div>
}

export default Result
