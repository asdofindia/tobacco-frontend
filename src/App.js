import React, { useState, useEffect } from 'react';
import Sidebar from 'react-sidebar';
import { ReactiveBase, DataSearch, MultiList, ReactiveList } from '@appbaseio/reactivesearch';

import Result from './components/Result';

import './App.css';

const mql = window.matchMedia(`(min-width: 800px)`);

const ShowFilters = ({docked, setSidebarOpen, children}) => {
  if (!docked) {
    return <button onClick={() => {
      setSidebarOpen(true)
    }}>{children}</button>
  } else {
    return null;
  }
}

const Filters = <> 
                  <DataSearch
                    componentId="search"
                    dataField={['Question']}
                    />

                  <MultiList
                    componentId="house"
                    dataField="House"
                    title="Houses"
                    />

                  <MultiList
                    componentId="department"
                    dataField="Department"
                    title="Department"
                    react={{
                      "and": ["member"]
                    }}
                  />

                  <MultiList
                    componentId="member"
                    dataField="Name"
                    title="Parliamentarian"
                    react={{
                      "and": ["department"]
                    }}
                  />

                  <MultiList
                    componentId="party"
                    dataField="Political Party"
                    title="Political Party"
                  />

                </>      


function App() {

  const [sidebarDocked, setSidebarDocked] = useState(mql.matches);
  const [sidebarOpen, setSidebarOpen] = useState(false);

  useEffect(() => {
    const mediaQueryChanged = () => {
      setSidebarDocked(mql.matches);
      setSidebarOpen(false);
    }
    mql.addListener(mediaQueryChanged);
    return () => {
      mql.removeListener(mediaQueryChanged);
    }
  })  

  const port = window.location.port ? `:${window.location.port}` : '';
  const url = window.location.hostname === "localhost" ? `http://localhost:9200` : `${window.location.protocol}//${window.location.hostname}${port}/api`;

  return (
    <div className="App">
      <ReactiveBase
        app="questions"
        url={url}
      >
        <Sidebar
          sidebar={Filters}
          open={sidebarOpen}
          docked={sidebarDocked}
          onSetOpen={setSidebarOpen}
          styles={{ sidebar: { background: "white" } }}
        >
          <div>
            <h1>Tobacco Questions</h1>
            <ShowFilters docked={sidebarDocked} setSidebarOpen={setSidebarOpen}>Choose Filters</ShowFilters>
          </div>
          <ReactiveList
            componentId="SearchResult"
            react={{
              and: ['search', 'house', 'department', 'member'],
            }}
            renderItem={res => <Result res={res} key={res.File}/>}
            pagination={true}
          />
        </Sidebar>   
      </ReactiveBase>        
    </div>
  );
}

export default App;
